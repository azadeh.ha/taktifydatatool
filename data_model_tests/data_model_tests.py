import unittest
import sys
import os
import inspect
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from datatool_api.models.DTDataset import DTDataset
from datatool_api.models.BaseTypes import DatasetMetadata
from custom_dataset_model import DTDatasetCustom
from custom_base_types import *


class TestDataModel(unittest.TestCase):

    test_json_path = 'test_dataset.json'
    dump_json_path = 'test_dataset_dump.json'
    expected_counts = {
        'info': 0
    }

    def testJSONLoad(self):
        """
        Test if the test_dataset.json is loaded correctly using the data model classes

        :return:
        """
        dataset = DTDatasetCustom(name='test_dataset', operatingMode='memory').load_from_json(self.test_json_path)
        self.assertIsNotNone(dataset.info)


    def testJSONDump(self):
        """
        Test if loaded dataset can be dumped correctly to disk using the data model classes

        :return:
        """
        dataset = DTDatasetCustom(name='test_dataset', operatingMode='memory').load_from_json(self.test_json_path)
        dataset.metadata = DatasetMetadata(datatoolName='testDatatool', datatoolVersion='testVersion',
                                           datatoolTag='testTag')
        dataset.to_json(json_path=self.dump_json_path, validate_sample_paths=False)

    def testSampleCount(self):
        """
        Test if dumped dataset can be loaded back correctly from disk using the data model classes and the elements count are as expected

        :return:
        """
        self.addCleanup(cleanup, self.dump_json_path)
        dataset = DTDatasetCustom(name='test_dataset', operatingMode='memory').load_from_json(self.dump_json_path)

        self.assertIsNotNone(dataset.info)


        self.assertEqual(self.expected_counts['info'], len(dataset.info))


def cleanup(dump_json_path):
    if os.path.exists(dump_json_path):
        os.remove(dump_json_path)


if __name__ == '__main__':
    with open('DataModel_TestReport.out', "w") as f:
        runner = unittest.TextTestRunner(f, verbosity=4)
        unittest.main(testRunner=runner)
