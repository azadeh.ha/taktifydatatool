#!/usr/bin/env bash

IMAGE_TAG="registry.gitlab.com/bonseyes/artifacts/data_tools/eda-tool:latest"
CONTAINER_NAME="EDA_Report"

PARAMS=""
INPUT_DIR=""
OUTPUT_DIR=""

function usage() {
  echo "Usage:

  source create_EDA_report.sh

  Required arguments:
    --input-dir, -i        Directory where the datatool output is stored (where dataset.json can be found)
    --output-dir, -o       Output directory where to store the EDA report


  Optional arguments:
    --report-type, -r       Type of report to generate, currently supported types are
                            1: Enhanced Dataprep EDA Report
    --operation-mode, -om,  Datatool operation mode [memory, disk, ramdisk]
  "
}

function realpath() {
    [[ "$1" = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

function make_decision() {
  if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
    if [ -z "$3" ]; then
      PARAMS="$PARAMS $1 $2"
    fi
  else
    echo "Error: Argument for $1 is missing" >&2
    exit 1
  fi
}

while (( $# )); do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    -om|--operation-mode)
      make_decision "$1" "$2"
      shift 2
      ;;
    -r|--report-type)
      make_decision "$1" "$2"
      shift 2
      ;;
    -o|--output-dir)
      make_decision "$1" "$2" "output"
      OUTPUT_DIR="$2"
      shift 2
      ;;
    -i|--input-dir)
      make_decision "$1" "$2" "input"
      INPUT_DIR="$2"
      shift 2
      ;;
    -*|--*=)
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
  esac
done

if [ -z "$OUTPUT_DIR" ] || [ -z "$INPUT_DIR" ]; then
  usage
  exit 1
fi

echo "Login to your gitlab docker registry to fetch the EDA report generation tool"
docker login registry.gitlab.com
docker pull "$IMAGE_TAG"

INPUT_DIR=$(realpath "$INPUT_DIR")

MOUNT_INPUT="-v $(realpath "$INPUT_DIR"):/input:ro"

mkdir -p "$OUTPUT_DIR"

echo "Running the reporting container"
command="docker run -t -d \
  $MOUNT_INPUT $MOUNT_OUTPUT \
  --name $CONTAINER_NAME $IMAGE_TAG"
eval "$command"

echo "Loading the datatool output and creating the report"
docker cp custom_dataset_model.py $"$CONTAINER_NAME":/app
docker cp custom_base_types.py $"$CONTAINER_NAME":/app
docker exec -it "$CONTAINER_NAME" bash -c "python3 create_report.py --input-dir /input --output-dir /output $PARAMS"
docker cp "$CONTAINER_NAME":/output/. "$OUTPUT_DIR"

echo "Removing the container"
docker rm -f "$CONTAINER_NAME"

echo "Process completed"