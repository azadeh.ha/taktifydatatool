from abc import ABC, abstractmethod
from ..data_validation.base_model import BaseModel


class StoragePointer:
	__slots__ = ["id", "type"]
	
	def __init__(self, object_id, class_type):
		self.id = object_id
		self.type = class_type


class StorageHandlerInterface(ABC):	
	@abstractmethod
	def write(self, object_id, data_category: str, data_object: BaseModel) -> StoragePointer:
		pass
	
	@abstractmethod
	def read(self, pointer: StoragePointer, remove=False) -> BaseModel:
		pass
	
	@abstractmethod
	def close(self):
		pass

	@abstractmethod
	def remove(self, pointer: StoragePointer) -> None:
		pass
