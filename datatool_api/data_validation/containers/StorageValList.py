from ..attribute import Attribute
from typing import Generic, MutableSequence, Iterator
from typing import Any, Iterable
from typing import TypeVar


_T = TypeVar("_T")
_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_VT_co = TypeVar("_VT_co", covariant=True)
_T_co = TypeVar("_T_co", covariant=True)


class StorageValList(MutableSequence[_T], Generic[_T]):
    def __init__(self, validator: Attribute = None, name: str = None, parent: object = None):
        self._name = name
        self._parent = parent
        self._validator = validator
        self._data = []

    def _get_storage_interface(self):
        if self._parent is None or getattr(self._parent, '_storageInterface', None) is None:
            raise ValueError('Parent object must be set in StorageValList and must have a set _storageInterface '
                             'attribute pointing to StorageIOInterface')
        else:
            return getattr(self._parent, '_storageInterface')

    def _validate(self, value: _T):
        if self._validator is not None:
            self._validator.validate('ValList Element', value)

    def append(self, value: _T) -> None:
        self._validate(value)
        pointer = self._get_storage_interface().write_to_storage(
            str(len(self._data)), (self._parent.__class__.__name__ + self._name), value)
        self._data.append(pointer)
        if self._parent is not None and self._name is not None and getattr(self._parent, self._name, None) is None:
            setattr(self._parent, self._name, self)

    def clear(self) -> None:
        for pointer in self._data:
            self._get_storage_interface().remove_from_storage(pointer)
        self._data.clear()

    def reverse(self) -> None:
        self._data.reverse()

    def add(self, value: _T) -> None:
        self.append(value)

    def item_count(self) -> int:
        return len(self._data)

    def __getitem__(self, i: int) -> _T:
        pointer = self._data[i]
        return self._get_storage_interface().read_from_storage(pointer)

    def __iter__(self) -> Iterator[_T_co]:
        for pointer in self._data:
            yield self._get_storage_interface().read_from_storage(pointer)

    def __setitem__(self, i: int, o: _T) -> None:
        self._validate(o)
        pointer = self._get_storage_interface().write_to_storage(
            str(i), (self._parent.__class__.__name__ + self._name), o)
        self._data.__setitem__(i, pointer)

    def __len__(self) -> int:
        return len(self._data)

    def __reversed__(self) -> Iterator[_T_co]:
        rev = reversed(self._data)
        for pointer in rev:
            yield self._get_storage_interface().read_from_storage(pointer)

    def __delitem__(self, i: int) -> None:
        raise NotImplementedError

    def pop(self, index: int = ...) -> _T:
        raise NotImplementedError

    def insert(self, index: int, value: _T) -> None:
        raise NotImplementedError

    def index(self, value: Any, start: int = ..., stop: int = ...) -> int:
        raise NotImplementedError

    def count(self, value: Any) -> int:
        raise NotImplementedError

    def extend(self, values: Iterable[_T]) -> None:
        raise NotImplementedError

    def remove(self, value: _T) -> None:
        raise NotImplementedError

    def __contains__(self, x: object) -> bool:
        raise NotImplementedError
