from ..attribute import Attribute
from typing import Generic, MutableSequence, Iterator
from typing import Any, Iterable
from typing import TypeVar


_T = TypeVar("_T")
_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_VT_co = TypeVar("_VT_co", covariant=True)
_T_co = TypeVar("_T_co", covariant=True)


class ValList(MutableSequence[_T], Generic[_T]):
    def __init__(self, validator: Attribute = None, name: str = None, parent: object = None):
        self._name = name
        self._parent = parent
        self._validator = validator
        self._data = []

    def _validate(self, value: _T):
        if self._validator is not None:
            self._validator.validate('ValList Element', value)

    def pop(self, index: int = ...) -> _T:
        return self._data.pop(index)

    def append(self, value: _T) -> None:
        self._validate(value)
        self._data.append(value)
        if self._parent is not None and self._name is not None and getattr(self._parent, self._name, None) is None:
            setattr(self._parent, self._name, self)

    def clear(self) -> None:
        self._data.clear()

    def insert(self, index: int, value: _T) -> None:
        self._validate(value)
        self._data.insert(index, value)
        if self._parent is not None and self._name is not None and getattr(self._parent, self._name, None) is None:
            setattr(self._parent, self._name, self)

    def reverse(self) -> None:
        self._data.reverse()

    def add(self, value: _T) -> None:
        self.append(value)

    def item_count(self) -> int:
        return len(self._data)

    def __getitem__(self, i: int) -> _T:
        return self._data.__getitem__(i)

    def __iter__(self) -> Iterator[_T_co]:
        return iter(self._data)

    def __setitem__(self, i: int, o: _T) -> None:
        self._validate(o)
        self._data.__setitem__(i, o)

    def __delitem__(self, i: int) -> None:
        self._data.__delitem__(i)

    def __len__(self) -> int:
        return len(self._data)

    def __reversed__(self) -> Iterator[_T_co]:
        return reversed(self._data)

    def index(self, value: Any, start: int = ..., stop: int = ...) -> int:
        raise NotImplementedError

    def count(self, value: Any) -> int:
        raise NotImplementedError

    def extend(self, values: Iterable[_T]) -> None:
        raise NotImplementedError

    def remove(self, value: _T) -> None:
        raise NotImplementedError

    def __contains__(self, x: object) -> bool:
        raise NotImplementedError
