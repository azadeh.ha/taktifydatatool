from .validator import DTDataValidator
from typing import Any


class Attribute:
    def __init__(self, **kwargs):
        self.constraints = kwargs

    def validate(self, name: str, value: Any) -> None:
        """
        Validates an attribute based on the constraints provided in the constructor,
        raises exception if the attribute value doesn't satisfy any constraint

        :param name: Name of the attribute
        :param value: Value of the attribute
        :return: None
        """
        if DTDataValidator.applyValidation is True:
            for key, target in self.constraints.items():
                validator = DTDataValidator.get_validator(key)
                validator(name, value, target)
