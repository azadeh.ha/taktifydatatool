from ...data_validation.base_model import BaseModel
from ...data_validation.attribute import Attribute


class BaseSample(BaseModel):
    id: str = Attribute(type=str, ne='')
    samplePath: str = Attribute(type=str, ne='')

    def __init__(self, id: str = None, samplePath: str = None, **kwargs):
        kwargs['id'] = id
        kwargs['samplePath'] = samplePath
        BaseModel.__init__(self, **kwargs)
