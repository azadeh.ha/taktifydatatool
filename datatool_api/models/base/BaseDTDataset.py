from __future__ import annotations
from abc import ABC, abstractmethod
import inspect
import json
import os.path
import pandas
from tqdm import tqdm
import ijson
from datetime import datetime
from enum import Enum
from ...data_validation.base_model import BaseModel
from ...data_validation.attribute import Attribute
from ...interfaces.StorageIOInterface import StorageIOInterface
from ...storage_handlers.MemoryStorageHandler import MemoryStorageHandler
from ...storage_handlers.RamdiskStorageHandler import RamdiskStorageHandler
from ...storage_handlers.DiskStorageHandler import DiskStorageHandler
from ...data_validation.containers.ValDict import ValDict
from ...data_validation.containers.ValList import ValList
from ...data_validation.containers.StorageValDict import StorageValDict
from ...data_validation.containers.StorageValList import StorageValList
from .BaseSample import BaseSample
from ..BaseTypes import DatasetMetadata
from typing import List, Tuple, Dict
import concurrent.futures


class BaseDTDataset(BaseModel, ABC):
    name: str = Attribute(type=str, ne='')
    operatingMode: str = Attribute(type=str, enum=['memory', 'disk', 'ramdisk'])
    metadata: DatasetMetadata = Attribute(type=DatasetMetadata, rsa=['datatoolName', 'datatoolVersion', 'datatoolTag'])

    def __init__(self, name: str, operatingMode: str, **kwargs):
        BaseModel.__init__(self, **kwargs)
        self.name = name
        self.operatingMode = operatingMode
        self.base_elements = ['name', 'operatingMode']

        if self.operatingMode == "memory":
            self._storageInterface = StorageIOInterface(storage_handler=MemoryStorageHandler(),
                                                        operatingMode=self.operatingMode)
        elif self.operatingMode == "disk":
            start_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
            operating_dir = f"{self.name}_{start_time}"
            self._storageInterface = StorageIOInterface(storage_handler=DiskStorageHandler(operating_dir),
                                                        operatingMode=self.operatingMode)
        else:
            self._storageInterface = StorageIOInterface(storage_handler=RamdiskStorageHandler(),
                                                        operatingMode=self.operatingMode)
        BaseModel.__init__(self, **kwargs)

    def __del__(self):
        if hasattr(self, '_storageInterface'):
            self._storageInterface.close()

    @staticmethod
    def _validate_sample_path(flag, root_dir, value):
        if flag is True and isinstance(value, BaseSample):
            sample_path = os.path.join(root_dir, value.samplePath)
            if not os.path.exists(sample_path) or os.path.isdir(sample_path):
                raise Exception('Sample path does not correspond to a file on disk')

    class _DatasetDumperUtils:
        @staticmethod
        def handle_list(attr_value, validator: Attribute, val_flag, root_dir):
            out = []
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for v in attr_value:
                out.append(BaseDTDataset._get_mappers('dump')[base_type](v, elem_validator, val_flag, root_dir))
            return out

        @staticmethod
        def handle_dict(attr_value, validator: Attribute, val_flag, root_dir):
            out = {}
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for k, v in attr_value.items():
                out[k] = BaseDTDataset._get_mappers('dump')[base_type](v, elem_validator, val_flag, root_dir)
            return out

        @staticmethod
        def handle_base_model(attr_value, validator: Attribute, val_flag, root_dir):
            BaseDTDataset._validate_sample_path(val_flag, root_dir, attr_value)
            return attr_value.dict()

        @staticmethod
        def handle_enum(attr_value, validator: Attribute, val_flag, root_dir):
            return attr_value.value

        @staticmethod
        def handle_others(attr_value, validator: Attribute, val_flag, root_dir):
            return attr_value

    class _DatasetLoaderUtils:
        @staticmethod
        def handle_val_list(input_instance: BaseModel, attr_name: str, input_list: list, validator: Attribute,
                            doSet=True):
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            out = validator.constraints.get('type')(elem_validator, '_' + attr_name, input_instance)
            for v in input_list:
                out.add(BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                pass
            else:
                return out

        @staticmethod
        def handle_list(input_instance: BaseModel, attr_name: str, input_list: list, validator: Attribute, doSet=True):
            out = []
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for v in input_list:
                out.append(BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                setattr(input_instance, attr_name, out)
            else:
                return out

        @staticmethod
        def handle_val_dict(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute,
                            doSet=True):
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            out = validator.constraints.get('type')(elem_validator, '_' + attr_name, input_instance)
            for k, v in input_dict.items():
                out.add(k, BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                pass
            else:
                return out

        @staticmethod
        def handle_dict(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute, doSet=True):
            out = {}
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for k, v in input_dict.items():
                out[k] = BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False)
            if doSet is True:
                setattr(input_instance, attr_name, out)
            else:
                return out

        @staticmethod
        def handle_base_model(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute,
                              doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, validator.constraints.get('type')().from_dict(input_dict))
            else:
                return validator.constraints.get('type')().from_dict(input_dict)

        @staticmethod
        def handle_enum(input_instance: BaseModel, attr_name: str, input_value: str, validator: Attribute, doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, validator.constraints.get('type')(input_value))
            else:
                return validator.constraints.get('type')(input_value)

        @staticmethod
        def handle_others(input_instance: BaseModel, attr_name: str, input_value: str, validator: Attribute,
                          doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, input_value)
            else:
                return input_value

    @staticmethod
    def _get_mappers(typeStr: str):
        return {
            'load': {
                list: BaseDTDataset._DatasetLoaderUtils.handle_list,
                ValList: BaseDTDataset._DatasetLoaderUtils.handle_val_list,
                StorageValList: BaseDTDataset._DatasetLoaderUtils.handle_val_list,
                dict: BaseDTDataset._DatasetLoaderUtils.handle_dict,
                ValDict: BaseDTDataset._DatasetLoaderUtils.handle_val_dict,
                StorageValDict: BaseDTDataset._DatasetLoaderUtils.handle_val_dict,
                BaseModel: BaseDTDataset._DatasetLoaderUtils.handle_base_model,
                Enum: BaseDTDataset._DatasetLoaderUtils.handle_enum,
                object: BaseDTDataset._DatasetLoaderUtils.handle_others
            },
            'dump': {
                list: BaseDTDataset._DatasetDumperUtils.handle_list,
                ValList: BaseDTDataset._DatasetDumperUtils.handle_list,
                StorageValList: BaseDTDataset._DatasetDumperUtils.handle_list,
                dict: BaseDTDataset._DatasetDumperUtils.handle_dict,
                ValDict: BaseDTDataset._DatasetDumperUtils.handle_dict,
                StorageValDict: BaseDTDataset._DatasetDumperUtils.handle_dict,
                BaseModel: BaseDTDataset._DatasetDumperUtils.handle_base_model,
                Enum: BaseDTDataset._DatasetDumperUtils.handle_enum,
                object: BaseDTDataset._DatasetDumperUtils.handle_others
            }
        }[typeStr]

    def to_json(self, json_path: str, validate_sample_paths: bool = True, example_mode: bool = False):
        """
        Serialize the dataset to a json file

        :param json_path: Path to the output json file
        :param example_mode: If set True, all dict, list elements will dump simple element only (for example purpose)
        :param validate_sample_paths: If Sample paths must be validated to ensure there is a physical sample file
                                      present
        :return: None
        """
        # Check for metadata
        if getattr(self, '_metadata', None) is None:
            raise Exception('Metadata for the dataset must be set, '
                            'please add metadata to dataset before dumping to json')
        else:
            if self.metadata.createdBy is None:
                self.metadata.createdBy = inspect.stack()[1].filename
            if self.metadata.creationTime is None:
                self.metadata.creationTime = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

        element_count = sum([1 for key in self.__class__.__validators__ if getattr(self, '_' + key, None) is not None])
        element_count = element_count - len(self.base_elements)

        print('Dumping the dataset JSON')
        output_dir = os.path.dirname(json_path)
        with open(json_path, 'w') as w:
            w.write('{')
            count = 0
            with tqdm(total=element_count, desc="Dataset Elements") as progress_bar:
                for key in self.__class__.__validators__:
                    value = getattr(self, '_' + key, None)
                    if value is not None and key not in self.base_elements:
                        progress_bar.set_postfix({"Element": key})
                        if count > 0:
                            w.write(',')
                        count += 1
                        if issubclass(self.__class__.__validators__[key].constraints.get('type', object), BaseModel):
                            out = self._get_mappers('dump')[BaseModel](value, self.__class__.__validators__[key],
                                                                       validate_sample_paths, output_dir)
                            w.write('"' + key + '":' + json.dumps(out))
                        elif issubclass(self.__class__.__validators__[key].constraints.get('type', object),
                                        (ValDict, StorageValDict, dict)):
                            w.write('"' + key + '":{')
                            elem_validator = self.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                                Attribute())
                            base_type = getattr(elem_validator, '_baseType', object)
                            elem_count = 0
                            for elem_key in tqdm(value, total=len(value)):
                                elem_val = value[elem_key]
                                if elem_count > 0:
                                    w.write(',')
                                elem_count += 1
                                w.write('"' + elem_key + '":')
                                out = self._get_mappers('dump')[base_type](elem_val, elem_validator,
                                                                           validate_sample_paths, output_dir)
                                w.write(json.dumps(out))
                                if example_mode is True:
                                    break
                            w.write('}')
                        elif issubclass(self.__class__.__validators__[key].constraints.get('type', object),
                                        (ValList, StorageValList, list)):
                            w.write('"' + key + '":[')
                            elem_validator = self.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                                Attribute())
                            base_type = getattr(elem_validator, '_baseType', object)
                            elem_count = 0
                            for elem_val in tqdm(value, total=len(value)):
                                if elem_count > 0:
                                    w.write(',')
                                elem_count += 1
                                out = self._get_mappers('dump')[base_type](elem_val, elem_validator,
                                                                           validate_sample_paths, output_dir)
                                w.write(json.dumps(out))
                                if example_mode is True:
                                    break
                            w.write(']')
                        elif issubclass(self.__class__.__validators__[key].constraints.get('type', object), Enum):
                            w.write('"' + key + '":"' + value.value + '"')
                        else:
                            w.write('"' + key + '":"' + value + '"')
                    progress_bar.update()
            w.write('}')

        print('Dataset JSON dumping completed!')

    @staticmethod
    def load_thread(input_instance, json_path, key):
        with open(json_path, 'rb') as r:
            if issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object), BaseModel):
                for v in ijson.items(r, key, use_float=True):
                    intermediate = input_instance.__class__.__validators__[key].constraints.get('type')().from_dict(v)
                    setattr(input_instance, key, intermediate)
            elif issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object),
                            (ValDict, StorageValDict)):
                elem_validator = input_instance.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                              Attribute())
                base_type = getattr(elem_validator, '_baseType', object)
                for k, v in ijson.kvitems(r, key, use_float=True):
                    elem = BaseDTDataset._get_mappers('load')[base_type](input_instance, k, v, elem_validator, False)
                    getattr(input_instance, key).add(k, elem)
            elif issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object), dict):
                elem_validator = input_instance.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                              Attribute())
                base_type = getattr(elem_validator, '_baseType', object)
                intermediate = {}
                for k, v in ijson.kvitems(r, key, use_float=True):
                    elem = BaseDTDataset._get_mappers('load')[base_type](input_instance, k, v, elem_validator, False)
                    intermediate[k] = elem
                setattr(input_instance, key, intermediate)
            elif issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object),
                            (ValList, StorageValList)):
                elem_validator = input_instance.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                              Attribute())
                base_type = getattr(elem_validator, '_baseType', object)
                for v in ijson.items(r, key + '.item', use_float=True):
                    elem = BaseDTDataset._get_mappers('load')[base_type](input_instance, 'list elem', v,
                                                                         elem_validator, False)
                    getattr(input_instance, key).add(elem)
            elif issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object), list):
                elem_validator = input_instance.__class__.__validators__[key].constraints.get('element_constraints',
                                                                                              Attribute())
                base_type = getattr(elem_validator, '_baseType', object)
                intermediate = []
                for v in ijson.items(r, key + '.item', use_float=True):
                    elem = BaseDTDataset._get_mappers('load')[base_type](input_instance, 'list elem', v,
                                                                         elem_validator, False)
                    intermediate.append(elem)
                setattr(input_instance, key, intermediate)
            elif issubclass(input_instance.__class__.__validators__[key].constraints.get('type', object), Enum):
                for v in ijson.items(r, key, use_float=True):
                    setattr(input_instance, key,
                            input_instance.__class__.__validators__[key].constraints.get('type')(v))
            else:
                for v in ijson.items(r, key, use_float=True):
                    setattr(input_instance, key, v)
            return input_instance

    def load_from_json(self, json_path: str, element_list: List[str] = None) -> BaseDTDataset:
        """
        Load a json serialized dataset

        :param json_path: Path to the input json file
        :param element_list: List containing the name of elements if only specific elements must be loaded
        :return: BaseDTDataset
        """
        print('Loading the dataset...')
        if element_list is None:
            element_list = [key for key in self.__class__.__validators__.keys() if key not in self.base_elements]

        with tqdm(total=len(element_list), desc="Dataset Elements") as progress_bar:
            futures = []
            with concurrent.futures.ThreadPoolExecutor(max_workers=len(element_list)) as executor:
                for key in self.__class__.__validators__:
                    if key in element_list:
                        future = executor.submit(BaseDTDataset.load_thread, *(self, json_path, key))
                        futures.append((key, future))

                for future in futures:
                    progress_bar.set_postfix({"Element": future[0]})
                    future[1].result()
                    progress_bar.update()

        print('Dataset loading completed!')
        return self

    @abstractmethod
    def to_pandas_frame(self, keep_original: bool = False, column_subset: List = None) -> List[Tuple[str,
                                                                                                     pandas.DataFrame]]:
        """
        Pandas dataframe generator for dataset

        :param keep_original: If original dataset needs to be kept in memory, if False, the original dataset object
        can be modified, by popping samples from it.
        :param column_subset: If only a subset of all columns are needed in the dataframe
        :return: List of (dataframe_name, pandas.Dataframe)

        This is Dataset data model specific and must be implemented by the creator of Dataset data model
        """
        pass

    @abstractmethod
    def to_pandas_frame_for_report(self) -> List[Tuple[str, pandas.DataFrame, List[str]]]:
        """
        Get the pandas dataframe which is used for report generation. This method changes the column names to the
        expected names for columns in the report and only exports the columns which are required in the report.

        In addition it also export a column name list along with each dataframe holding the columns names which should
        be used to generate the interaction plots in the report.

        :return: List of (dataframe_name, pandas.Dataframe, List[columns_needed_for_interaction_plots])

        This is Dataset data model specific and must be implemented by the creator of Dataset data model
        """
        pass
