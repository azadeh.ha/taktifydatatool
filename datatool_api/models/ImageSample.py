from .BaseTypes import *
from .Subject import Subject
from .Object import Object
from ..data_validation.containers.ValList import ValList
from ..data_validation.containers.ValDict import ValDict
from .base.BaseSample import BaseSample


class ImageSample(BaseSample):
    subjects: ValList[Subject] = Attribute(type=ValList, min_items=1,
                                           element_constraints=Attribute(type=Subject, msa=1))
    objects: ValList[Object] = Attribute(type=ValList, min_items=1,
                                         element_constraints=Attribute(type=Object, msa=1))
    experimentals: ValDict[str, ExperimentalType] = Attribute(type=ValDict, min_items=1,
                                                              element_constraints=Attribute(type=ExperimentalType,
                                                                                            asa=True)
                                                              )
    metadata: ImageSampleMetadata = Attribute(type=ImageSampleMetadata, msa=1)

    def __init__(self, id: str = None, samplePath: str = None, subjects: ValList[Subject] = None,
                 objects: ValList[Object] = None, experimentals: ValDict[str, ExperimentalType] = None,
                 metadata: ImageSampleMetadata = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)
