from ..data_validation.base_model import BaseModel, Attribute
from .Categories import Categories
from typing import Any
from numbers import Number
from ..data_validation.containers.ValDict import ValDict
from ..data_validation.containers.ValList import ValList


class ActionUnit(BaseModel):
    label: Categories.ActionUnit = Attribute(type=Categories.ActionUnit)
    activated: bool = Attribute(type=bool)
    phase: Categories.ActionUnitPhase = Attribute(type=Categories.ActionUnitPhase)
    intensity: Categories.ActionUnitIntensity = Attribute(type=Categories.ActionUnitIntensity)
    symmetry: Categories.ActionUnitSymmetry = Attribute(type=Categories.ActionUnitSymmetry)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.ActionUnit = None, activated: bool = None,
                 phase: Categories.ActionUnitPhase = None, intensity: Categories.ActionUnitIntensity = None,
                 symmetry: Categories.ActionUnitSymmetry = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class ActionUnits(BaseModel):
    AU_10: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_11: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_12: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_13: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_14: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_15: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_16: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_17: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_18: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_19: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_20: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_21: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_22: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_23: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_24: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_25: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_26: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_27: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_28: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_29: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_30: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_31: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_32: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_33: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_34: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_35: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_36: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_37: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_38: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_39: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_40: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_41: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_42: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_43: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_44: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_45: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_46: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_01: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_02: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_04: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_05: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_06: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_07: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_08: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_09: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_51: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_52: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_53: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_54: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_55: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_56: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_57: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_58: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M55: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M56: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M57: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M59: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M60: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M83: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_61: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_62: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_63: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_64: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_65: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_66: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_69: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M61: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M62: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M68: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_M69: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_70: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_71: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_72: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_73: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_74: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_50: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_80: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_81: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_82: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_84: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_85: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_91: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_92: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_97: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])
    AU_98: ActionUnit = Attribute(type=ActionUnit, rsa=['activated'])

    def __init__(self, AU_10: ActionUnit = None, AU_11: ActionUnit = None, AU_12: ActionUnit = None,
                 AU_13: ActionUnit = None, AU_14: ActionUnit = None, AU_15: ActionUnit = None, AU_16: ActionUnit = None,
                 AU_17: ActionUnit = None, AU_18: ActionUnit = None, AU_19: ActionUnit = None, AU_20: ActionUnit = None,
                 AU_21: ActionUnit = None, AU_22: ActionUnit = None, AU_23: ActionUnit = None, AU_24: ActionUnit = None,
                 AU_25: ActionUnit = None, AU_26: ActionUnit = None, AU_27: ActionUnit = None, AU_28: ActionUnit = None,
                 AU_29: ActionUnit = None, AU_30: ActionUnit = None, AU_31: ActionUnit = None, AU_32: ActionUnit = None,
                 AU_33: ActionUnit = None, AU_34: ActionUnit = None, AU_35: ActionUnit = None, AU_36: ActionUnit = None,
                 AU_37: ActionUnit = None, AU_38: ActionUnit = None, AU_39: ActionUnit = None, AU_40: ActionUnit = None,
                 AU_41: ActionUnit = None, AU_42: ActionUnit = None, AU_43: ActionUnit = None, AU_44: ActionUnit = None,
                 AU_45: ActionUnit = None, AU_46: ActionUnit = None, AU_01: ActionUnit = None, AU_02: ActionUnit = None,
                 AU_04: ActionUnit = None, AU_05: ActionUnit = None, AU_06: ActionUnit = None, AU_07: ActionUnit = None,
                 AU_08: ActionUnit = None, AU_09: ActionUnit = None, AU_51: ActionUnit = None, AU_52: ActionUnit = None,
                 AU_53: ActionUnit = None, AU_54: ActionUnit = None, AU_55: ActionUnit = None, AU_56: ActionUnit = None,
                 AU_57: ActionUnit = None, AU_58: ActionUnit = None, AU_M55: ActionUnit = None,
                 AU_M56: ActionUnit = None, AU_M57: ActionUnit = None, AU_M59: ActionUnit = None,
                 AU_M60: ActionUnit = None, AU_M83: ActionUnit = None, AU_61: ActionUnit = None,
                 AU_62: ActionUnit = None, AU_63: ActionUnit = None, AU_64: ActionUnit = None, AU_65: ActionUnit = None,
                 AU_66: ActionUnit = None, AU_69: ActionUnit = None, AU_M61: ActionUnit = None,
                 AU_M62: ActionUnit = None, AU_M68: ActionUnit = None, AU_M69: ActionUnit = None,
                 AU_70: ActionUnit = None, AU_71: ActionUnit = None, AU_72: ActionUnit = None, AU_73: ActionUnit = None,
                 AU_74: ActionUnit = None, AU_50: ActionUnit = None, AU_80: ActionUnit = None, AU_81: ActionUnit = None,
                 AU_82: ActionUnit = None, AU_84: ActionUnit = None, AU_85: ActionUnit = None, AU_91: ActionUnit = None,
                 AU_92: ActionUnit = None, AU_97: ActionUnit = None, AU_98: ActionUnit = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Emotion(BaseModel):
    label: Categories.Emotion = Attribute(type=Categories.Emotion)
    activated: bool = Attribute(type=bool)
    arousal: Number = Attribute(type=Number, ge=0.0, le=1.0)
    valance: Number = Attribute(type=Number, ge=-1.0, le=1.0)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.Emotion = None, activated: bool = None, arousal: Number = None,
                 valance: Number = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Emotions(BaseModel):
    anger: Emotion = Attribute(type=Emotion, rsa=['activated'])
    disgust: Emotion = Attribute(type=Emotion, rsa=['activated'])
    fear: Emotion = Attribute(type=Emotion, rsa=['activated'])
    happiness: Emotion = Attribute(type=Emotion, rsa=['activated'])
    neutral: Emotion = Attribute(type=Emotion, rsa=['activated'])
    sadness: Emotion = Attribute(type=Emotion, rsa=['activated'])
    surprise: Emotion = Attribute(type=Emotion, rsa=['activated'])
    contempt: Emotion = Attribute(type=Emotion, rsa=['activated'])

    def __init__(self, anger: Emotion = None, disgust: Emotion = None, fear: Emotion = None, happiness: Emotion = None,
                 neutral: Emotion = None, sadness: Emotion = None, surprise: Emotion = None, contempt: Emotion = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Gender(BaseModel):
    label: Categories.Gender = Attribute(type=Categories.Gender)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.Gender = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Age(BaseModel):
    years: Number = Attribute(type=Number, ge=0, le=120)
    group: Categories.AgeGroup = Attribute(type=Categories.AgeGroup)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, years: Number = None, group: Categories.AgeGroup = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Ethnicity(BaseModel):
    label: Categories.Ethnicity = Attribute(type=Categories.Ethnicity)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.Ethnicity = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Nationality(BaseModel):
    label: str = Attribute(type=str)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: str = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class BoundingBox(BaseModel):
    region: Categories.BoundingBoxRegion = Attribute(type=Categories.BoundingBoxRegion)
    topX: Number = Attribute(type=Number)
    topY: Number = Attribute(type=Number)
    w: Number = Attribute(type=Number, gt=0)
    h: Number = Attribute(type=Number, gt=0)
    area: Number = Attribute(type=Number, gt=0)

    def __init__(self, region: Categories.BoundingBoxRegion = None, topX: Number = None,
                 topY: Number = None, w: Number = None,
                 h: Number = None, area: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Cuboid(BaseModel):
    region: Categories.BoundingBoxRegion = Attribute(type=Categories.BoundingBoxRegion)
    topX: Number = Attribute(type=Number)
    topY: Number = Attribute(type=Number)
    topZ: Number = Attribute(type=Number)
    w: Number = Attribute(type=Number, gt=0)
    h: Number = Attribute(type=Number, gt=0)
    d: Number = Attribute(type=Number, gt=0)
    volume: Number = Attribute(type=Number, gt=0)

    def __init__(self, region: Categories.BoundingBoxRegion = None, topX: Number = None,
                 topY: Number = None, topZ: Number = None, w: Number = None,
                 h: Number = None, d: Number = None, volume: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Segmentation(BaseModel):
    region: Categories.SegmentationRegion = Attribute(type=Categories.SegmentationRegion)
    segmentationType: Categories.SegmentationType = Attribute(type=Categories.SegmentationType)
    value: list = Attribute(type=list, min_items=1)

    def __init__(self, region: Categories.SegmentationRegion = None,
                 segmentationType: Categories.SegmentationType = None, value: list = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Landmark2D(BaseModel):
    region: Categories.LandmarkRegion = Attribute(type=Categories.LandmarkRegion)
    index: int = Attribute(type=int, ge=0)
    x: Number = Attribute(type=Number)
    y: Number = Attribute(type=Number)
    visible: bool = Attribute(type=bool)

    def __init__(self, region: Categories.LandmarkRegion = None, index: int = None,
                 x: Number = None, y: Number = None, visible: bool = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Landmark3D(BaseModel):
    region: Categories.LandmarkRegion = Attribute(type=Categories.LandmarkRegion)
    index: int = Attribute(type=int, ge=0)
    x: Number = Attribute(type=Number)
    y: Number = Attribute(type=Number)
    z: Number = Attribute(type=Number)
    visible: bool = Attribute(type=bool)

    def __init__(self, region: Categories.LandmarkRegion = None, index: int = None, x: Number = None,
                 y: Number = None, z: Number = None, visible: bool = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Drowsy(BaseModel):
    label: bool = Attribute(type=bool)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: bool = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class FaceOrientation(BaseModel):
    label: Categories.FaceOrientation = Attribute(type=Categories.FaceOrientation)

    def __init__(self, label: Categories.FaceOrientation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Face(BaseModel):
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topY', 'topZ', 'w', 'h', 'd', 'volume'])
    landmarks2d: ValList[Landmark2D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark2D,
                                                                               rsa=['index', 'x', 'y']))
    landmarks3d: ValList[Landmark3D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark3D,
                                                                               rsa=['index', 'x', 'y', 'z']))
    drowsy: Drowsy = Attribute(type=Drowsy, rsa=['label'])
    orientation: FaceOrientation = Attribute(type=FaceOrientation, rsa=['label'])
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])

    def __init__(self, boundingBox: BoundingBox = None, cuboid: Cuboid = None, landmarks2d: ValList[Landmark2D] = None,
                 landmarks3d: ValList[Landmark3D] = None, drowsy: Drowsy = None, orientation: FaceOrientation = None,
                 segmentation: Segmentation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Hand(BaseModel):
    label: Categories.HandLabel = Attribute(type=Categories.HandLabel)
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topY', 'topZ', 'w', 'h', 'd', 'volume'])
    landmarks2d: ValList[Landmark2D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark2D,
                                                                               rsa=['index', 'x', 'y']))
    landmarks3d: ValList[Landmark3D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark3D,
                                                                               rsa=['index', 'x', 'y', 'z']))
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])

    def __init__(self, label: Categories.HandLabel = None, boundingBox: BoundingBox = None, cuboid: Cuboid = None,
                 landmarks2d: ValList[Landmark2D] = None, landmarks3d: ValList[Landmark3D] = None,
                 segmentation: Segmentation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Hands(BaseModel):
    leftHand: Hand = Attribute(type=Hand, osa=['boundingBox', 'cuboid', 'landmarks2d', 'landmarks3d'])
    rightHand: Hand = Attribute(type=Hand, osa=['boundingBox', 'cuboid', 'landmarks2d', 'landmarks3d'])


class Foot(BaseModel):
    label: Categories.FootLabel = Attribute(type=Categories.FootLabel)
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topX', 'topZ', 'w', 'h', 'd', 'volume'])
    landmarks2d: ValList[Landmark2D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark2D,
                                                                               rsa=['index', 'x', 'y']))
    landmarks3d: ValList[Landmark3D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark3D,
                                                                               rsa=['index', 'x', 'y', 'z']))
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])

    def __init__(self, label: Categories.FootLabel = None, boundingBox: BoundingBox = None, cuboid: Cuboid = None,
                 landmarks2d: ValList[Landmark2D] = None, landmarks3d: ValList[Landmark3D] = None,
                 segmentation: Segmentation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Feet(BaseModel):
    leftFoot: Foot = Attribute(type=Foot, osa=['boundingBox', 'cuboid', 'landmarks2d', 'landmarks3d'])
    rightFoot: Foot = Attribute(type=Foot, osa=['boundingBox', 'cuboid', 'landmarks2d', 'landmarks3d'])


class Headpose(BaseModel):
    yaw: Number = Attribute(type=Number, ge=-90.0, le=90.0)
    pitch: Number = Attribute(type=Number, ge=-90.0, le=90.0)
    roll: Number = Attribute(type=Number, ge=-90.0, le=90.0)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, yaw: Number = None, pitch: Number = None, roll: Number = None,
                 confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class EyesOrientation(BaseModel):
    label: Categories.EyesOrientation = Attribute(type=Categories.EyesOrientation)

    def __init__(self, label: Categories.EyesOrientation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class EyesClosed(BaseModel):
    label: Categories.EyesClosed = Attribute(type=Categories.EyesClosed)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.EyesClosed = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class EyesOnRoad(BaseModel):
    label: bool = Attribute(type=bool)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: bool = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Eye(BaseModel):
    label: Categories.EyeLabel = Attribute(type=Categories.EyeLabel)
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topY', 'topZ', 'w', 'h', 'd', 'volume'])
    landmarks2d: ValList[Landmark2D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark2D,
                                                                               rsa=['index', 'x', 'y']))
    landmarks3d: ValList[Landmark3D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark3D,
                                                                               rsa=['index', 'x', 'y', 'z']))
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])

    def __init__(self, label: Categories.EyeLabel = None, boundingBox: BoundingBox = None, cuboid: Cuboid = None,
                 landmarks2d: ValList[Landmark2D] = None, landmarks3d: ValList[Landmark3D] = None,
                 segmentation: Segmentation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Eyes(BaseModel):
    leftEye: Eye = Attribute(type=Eye, osa=['landmarks2d', 'landmarks3d'])
    rightEye: Eye = Attribute(type=Eye, osa=['landmarks2d', 'landmarks3d'])
    orientation: EyesOrientation = Attribute(type=EyesOrientation, rsa=['label'])
    closed: EyesClosed = Attribute(type=EyesClosed, rsa=['label'])
    onRoad: EyesOnRoad = Attribute(type=EyesOnRoad, rsa=['label'])

    def __init__(self, leftEye: Eye = None, rightEye: Eye = None, orientation: EyesOrientation = None,
                 closed: EyesClosed = None, onRoad: EyesOnRoad = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Gaze(BaseModel):
    label: Categories.EyeLabel = Attribute(type=Categories.EyeLabel)
    yaw: Number = Attribute(type=Number, ge=-90.0, le=90.0)
    pitch: Number = Attribute(type=Number, ge=-90.0, le=90.0)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.EyeLabel = None, yaw: Number = None,
                 pitch: Number = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class EyeGaze(BaseModel):
    leftEye: Gaze = Attribute(type=Gaze, rsa=['yaw', 'pitch'])
    rightEye: Gaze = Attribute(type=Gaze, rsa=['yaw', 'pitch'])

    def __init__(self, leftEye: Gaze = None, rightEye: Gaze = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Pain(BaseModel):
    AFF: Categories.PainAFF = Attribute(type=Categories.PainAFF)
    SEN: Categories.PainSEN = Attribute(type=Categories.PainSEN)
    OPR: Categories.PainOPR = Attribute(type=Categories.PainOPR)
    VAS: int = Attribute(type=int, ge=0)
    PSPI: int = Attribute(type=int, ge=0)

    def __init__(self, AFF: Categories.PainAFF = None, SEN: Categories.PainSEN = None, OPR: Categories.PainOPR = None,
                 VAS: int = None, PSPI: int = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class FacialHair(BaseModel):
    label: Categories.FacialHair = Attribute(type=Categories.FacialHair)

    def __init__(self, label: Categories.FacialHair = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Glasses(BaseModel):
    label: Categories.Glasses = Attribute(type=Categories.Glasses)

    def __init__(self, label: Categories.Glasses = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class HairStyle(BaseModel):
    label: Categories.HairStyle = Attribute(type=Categories.HairStyle)

    def __init__(self, label: Categories.HairStyle = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class HairColor(BaseModel):
    label: Categories.HairColor = Attribute(type=Categories.HairColor)

    def __init__(self, label: Categories.HairColor = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class SkinType(BaseModel):
    label: Categories.SkinType = Attribute(type=Categories.SkinType)

    def __init__(self, label: Categories.SkinType = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Clothes(BaseModel):
    tShirt: bool = Attribute(type=bool)
    pulloverJacket: bool = Attribute(type=bool)
    thickWinterJacket: bool = Attribute(type=bool)
    hat: bool = Attribute(type=bool)
    baseballCap: bool = Attribute(type=bool)
    hoodie: bool = Attribute(type=bool)
    mask: bool = Attribute(type=bool)

    def __init__(self, tShirt: bool = None, pulloverJacket: bool = None, thickWinterJacket: bool = None,
                 hat: bool = None, baseballCap: bool = None, hoodie: bool = None, mask: bool = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Transients(BaseModel):
    facialHair: FacialHair = Attribute(type=FacialHair, rsa=['label'])
    glasses: Glasses = Attribute(type=Glasses, rsa=['label'])
    hairStyle: HairStyle = Attribute(type=HairStyle, rsa=['label'])
    hairColor: HairColor = Attribute(type=HairColor, rsa=['label'])
    skinType: SkinType = Attribute(type=SkinType, rsa=['label'])
    clothes: Clothes = Attribute(type=Clothes, msa=1)

    def __init__(self, facialHair: FacialHair = None, glasses: Glasses = None, hairStyle: HairStyle = None,
                 hairColor: HairColor = None, skinType: SkinType = None, clothes: Clothes = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class BodyType(BaseModel):
    label: Categories.BodyType = Attribute(type=Categories.BodyType)

    def __init__(self, label: Categories.BodyType = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class ScaleFactor(BaseModel):
    value: Number = Attribute(type=Number, gt=0.0)

    def __init__(self, value: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Body(BaseModel):
    bodyType: BodyType = Attribute(type=BodyType, rsa=['label'])
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topY', 'topZ', 'w', 'h', 'd', 'volume'])
    landmarks2d: ValList[Landmark2D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark2D,
                                                                               rsa=['index', 'x', 'y']))
    landmarks3d: ValList[Landmark3D] = Attribute(type=ValList, min_items=1,
                                                 element_constraints=Attribute(type=Landmark3D,
                                                                               rsa=['index', 'x', 'y', 'z']))
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])

    def __init__(self, bodyType: BodyType = None, boundingBox: BoundingBox = None, cuboid: Cuboid = None,
                 landmarks2d: ValList[Landmark2D] = None, landmarks3d: ValList[Landmark3D] = None,
                 segmentation: Segmentation = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class HandsOnWheel(BaseModel):
    label: Categories.HandsOnWheel = Attribute(type=Categories.HandsOnWheel)

    def __init__(self, label: Categories.HandsOnWheel = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class Activity(BaseModel):
    handsOnWheel: HandsOnWheel = Attribute(type=HandsOnWheel, rsa='label')


class ExperimentalType(BaseModel):
    id: str = Attribute(type=str, ne='')
    data: ValDict[str, Any] = Attribute(type=ValDict, min_items=1)

    def __init__(self, id: str = None, data: ValDict[str, Any] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class SkeletonEdge(BaseModel):
    skeletonType: Categories.SkeletonType = Attribute(type=Categories.SkeletonType)
    region: Categories.SkeletonRegion = Attribute(type=Categories.SkeletonRegion)
    first: int = Attribute(type=int, ge=0)
    second: int = Attribute(type=int, ge=0)

    def __init__(self, skeletonType: Categories.SkeletonType = None, region: Categories.SkeletonRegion = None,
                 first: int = None, second: int = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class LandmarkLabel(BaseModel):
    landmarkType: Categories.LandmarkType = Attribute(type=Categories.LandmarkType)
    region: Categories.LandmarkRegion = Attribute(type=Categories.LandmarkRegion)
    index: int = Attribute(type=int, ge=0)
    labelString: str = Attribute(type=str, ne='')

    def __init__(self, landmarkType: Categories.LandmarkType = None, region: Categories.LandmarkRegion = None,
                 index: int = None, labelString: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalFace(BaseModel):
    landmarkMap2d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    landmarkMap3d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    skeleton2d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )
    skeleton3d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )

    def __init__(self, landmarkMap2d: ValList[LandmarkLabel] = None, landmarkMap3d: ValList[LandmarkLabel] = None,
                 skeleton2d: ValList[SkeletonEdge] = None, skeleton3d: ValList[SkeletonEdge] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalBody(BaseModel):
    landmarkMap2d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    landmarkMap3d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    skeleton2d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )
    skeleton3d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )

    def __init__(self, landmarkMap2d: ValList[LandmarkLabel] = None, landmarkMap3d: ValList[LandmarkLabel] = None,
                 skeleton2d: ValList[SkeletonEdge] = None, skeleton3d: ValList[SkeletonEdge] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalEye(BaseModel):
    label: Categories.EyeLabel = Attribute(type=Categories.EyeLabel)
    landmarkMap2d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    landmarkMap3d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    skeleton2d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )
    skeleton3d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )

    def __init__(self, label: Categories.EyeLabel = None, landmarkMap2d: ValList[LandmarkLabel] = None,
                 landmarkMap3d: ValList[LandmarkLabel] = None, skeleton2d: ValList[SkeletonEdge] = None,
                 skeleton3d: ValList[SkeletonEdge] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalHand(BaseModel):
    label: Categories.HandLabel = Attribute(type=Categories.HandLabel)
    landmarkMap2d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    landmarkMap3d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    skeleton2d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )
    skeleton3d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )

    def __init__(self, label: Categories.HandLabel = None, landmarkMap2d: ValList[LandmarkLabel] = None,
                 landmarkMap3d: ValList[LandmarkLabel] = None, skeleton2d: ValList[SkeletonEdge] = None,
                 skeleton3d: ValList[SkeletonEdge] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalFoot(BaseModel):
    label: Categories.FootLabel = Attribute(type=Categories.FootLabel)
    landmarkMap2d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    landmarkMap3d: ValList[LandmarkLabel] = Attribute(type=ValList, min_items=1,
                                                      element_constraints=Attribute(
                                                          type=LandmarkLabel, rsa=['index', 'labelString'])
                                                      )
    skeleton2d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )
    skeleton3d: ValList[SkeletonEdge] = Attribute(type=ValList, min_items=1,
                                                  element_constraints=Attribute(
                                                      type=SkeletonEdge, rsa=['first', 'second'])
                                                  )

    def __init__(self, label: Categories.FootLabel = None, landmarkMap2d: ValList[LandmarkLabel] = None,
                 landmarkMap3d: ValList[LandmarkLabel] = None, skeleton2d: ValList[SkeletonEdge] = None,
                 skeleton3d: ValList[SkeletonEdge] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalEyes(BaseModel):
    leftEye: GlobalEye = Attribute(type=GlobalEye, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                        'skeleton3d'])
    rightEye: GlobalEye = Attribute(type=GlobalEye, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                         'skeleton3d'])

    def __init__(self, leftEye: GlobalEye = None, rightEye: GlobalEye = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalHands(BaseModel):
    leftHand: GlobalHand = Attribute(type=GlobalHand, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                           'skeleton3d'])
    rightHand: GlobalHand = Attribute(type=GlobalHand, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                            'skeleton3d'])

    def __init__(self, leftHand: GlobalHand = None, rightHand: GlobalHand = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class GlobalFeet(BaseModel):
    leftFoot: GlobalFoot = Attribute(type=GlobalFoot, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                           'skeleton3d'])
    rightFoot: GlobalFoot = Attribute(type=GlobalFoot, osa=['landmarkMap2d', 'landmarkMap3d', 'skeleton2d',
                                                            'skeleton3d'])

    def __init__(self, leftFoot: GlobalFoot = None, rightFoot: GlobalFoot = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class RecordingInfo(BaseModel):
    date: str = Attribute(type=str, date_time_format='%Y-%m-%d')
    time: str = Attribute(type=str, date_time_format='%H:%M:%S')

    def __init__(self, date: str = None, time: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class ImageSampleMetadata(BaseModel):
    sessionId: str = Attribute(type=str, ne='')
    fromSequence: bool = Attribute(type=bool)
    frameNumber: int = Attribute(type=int, ge=1)
    cameraId: str = Attribute(type=str, ne='')
    colorSpace: Categories.ColorSpace = Attribute(type=Categories.ColorSpace)
    recordingInfo: RecordingInfo = Attribute(type=RecordingInfo, msa=1)
    indoorLight: Categories.IndoorLight = Attribute(type=Categories.IndoorLight)
    outdoorLight: Categories.OutdoorLight = Attribute(type=Categories.OutdoorLight)
    dayTime: Categories.DayTime = Attribute(type=Categories.DayTime)
    weather: Categories.Weather = Attribute(type=Categories.Weather)

    def __init__(self, sessionId: str = None, fromSequence: bool = None, frameNumber: int = None, cameraId: str = None,
                 colorSpace: Categories.ColorSpace = None, recordingInfo: RecordingInfo = None,
                 indoorLight: Categories.IndoorLight = None, outdoorLight: Categories.OutdoorLight = None,
                 dayTime: Categories.DayTime = None, weather: Categories.Weather = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class VideoSampleMetadata(BaseModel):
    fps: Number = Attribute(type=Number, gt=0)
    cameraId: str = Attribute(type=str, ne='')
    colorSpace: Categories.ColorSpace = Attribute(type=Categories.ColorSpace)
    recordingInfo: RecordingInfo = Attribute(type=RecordingInfo, msa=1)
    indoorLight: Categories.IndoorLight = Attribute(type=Categories.IndoorLight)
    outdoorLight: Categories.OutdoorLight = Attribute(type=Categories.OutdoorLight)
    dayTime: Categories.DayTime = Attribute(type=Categories.DayTime)
    weather: Categories.Weather = Attribute(type=Categories.Weather)

    def __init__(self, fps: Number = None, cameraId: str = None, colorSpace: Categories.ColorSpace = None,
                 recordingInfo: RecordingInfo = None, indoorLight: Categories.IndoorLight = None,
                 outdoorLight: Categories.OutdoorLight = None, dayTime: Categories.DayTime = None,
                 weather: Categories.Weather = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class AudioKeyword(BaseModel):
    label: Categories.AudioKeyword = Attribute(type=Categories.AudioKeyword)
    confidence: Number = Attribute(type=Number, ge=0.0, le=1.0)

    def __init__(self, label: Categories.AudioKeyword = None, confidence: Number = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class AudioSampleMetadata(BaseModel):
    kbps: Number = Attribute(type=Number, gt=0)
    recordingInfo: RecordingInfo = Attribute(type=RecordingInfo, msa=1)

    def __init__(self, kbps: Number = None, recordingInfo: RecordingInfo = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class CameraCalibration(BaseModel):
    cameraId: str = Attribute(type=str, ne='')
    cameraMatrix: list = Attribute(type=list, min_items=1)
    cameraDistortion: list = Attribute(type=list, min_items=1)
    cameraTranslation: list = Attribute(type=list, min_items=1)
    cameraRotation: list = Attribute(type=list, min_items=1)

    def __init__(self, cameraId: str = None, cameraMatrix: list = None, cameraDistortion: list = None,
                 cameraTranslation: list = None, cameraRotation: list = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)


class DatasetMetadata(BaseModel):
    datatoolName: str = Attribute(type=str, ne='')
    datatoolVersion: str = Attribute(type=str, ne='')
    datatoolTag: str = Attribute(type=str, ne='')
    createdBy: str = Attribute(type=str, ne='')
    creationTime: str = Attribute(type=str, date_time_format='%Y-%m-%d %H:%M:%S')
    gitHash: str = Attribute(type=str, ne='')

    def __init__(self, datatoolName: str = None, datatoolVersion: str = None, datatoolTag: str = None,
                 createdBy: str = None, creationTime: str = None, gitHash: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)
