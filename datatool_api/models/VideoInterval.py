from ..data_validation.attribute import Attribute
from ..data_validation.base_model import BaseModel
from .Subject import Subject
from .Object import Object
from ..data_validation.containers.ValList import ValList


class VideoInterval(BaseModel):
    id: str = Attribute(type=str, ne='')
    index: int = Attribute(type=int, ge=0)
    startFrame: int = Attribute(type=int, gt=0)
    endFrame: int = Attribute(type=int, gt=0)
    subjects: ValList[Subject] = Attribute(type=ValList, min_items=1,
                                           element_constraints=Attribute(type=Subject, msa=1))
    objects: ValList[Object] = Attribute(type=ValList, min_items=1,
                                         element_constraints=Attribute(type=Object, msa=1))

    def __init__(self, id: str = None, index: int = None, startFrame: int = None, endFrame: int = None,
                 subjects: ValList[Subject] = None, objects: ValList[Object] = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)
