from common.schema_validation.validate_schema import validate_schema
from common.data_processing.source_data_storage_resolver import SourceDataStorageResolver
from common.data_processing.source_data_processor import DataProcessor
from source_data_parser import SourceDataParser
from datatool_patch import DatatoolPatch
import argparse
import os


def main():
    parser = argparse.ArgumentParser(description='Datatool starter')

    # Required Arguments
    parser.add_argument("--output-dir", "-o",  required=True,
                        help="Directory where to dump the output of datatool")

    # Optional Arguments to process specific version and tags
    parser.add_argument("--version", "-v",
                        help="Version of the dataset, "
                             "this overrides the current_version set in config_files/version_config.yml")
    parser.add_argument("--tags", "-t", nargs='*',
                        help="List of datatool tags which need to be processed, "
                             "all tags for the version are processed if none specified")

    # Optional arguments to override operational parameters
    parser.add_argument("--operation-mode", "-om", choices=['memory', 'disk', 'ramdisk'],
                        help="Operation mode for the datatool, "
                             "this overrides the mode set in config_files/operation_config.yml")
    parser.add_argument("--validate-sample-paths", "-vl", choices=['y', 'n'],
                        help="If sample paths must be validated while dumping the dataset json to disk "
                             "this overrides the value set in config_files/operation_config.yml")

    # Advance optional arguments for overriding storage related parameters
    parser.add_argument("--storage", "-s", choices=['synology', 's3', 'local'],
                        help="Type of storage, "
                             "this value overrides the storage set in config_files/storage_config.yml")
    parser.add_argument("--storage-loc", "-l",
                        help="Address for remote storage (only used if storage is of type synology), "
                             "this value overrides the storage_loc set in config_files/storage_config.yml")
    parser.add_argument("--storage-root", "-r", help="Root directory holding the dataset, "
                                                     "For local storage: Absolute path to the dataset root directory, "
                                                     "For synology storage: Path to the synology root directory, "
                                                     "For s3 storage: Bucket name of the bucket holding the dataset"
                        )

    # Parse arguments
    args = parser.parse_args()

    # Read the configs and validate their schemas
    storage_config = validate_schema('config_files/storage_config.yml', 'config_schemas/storage_schema.yml')
    version_config = validate_schema('config_files/version_config.yml', 'config_schemas/version_schema.yml')
    operation_config = validate_schema('config_files/operation_config.yml', 'config_schemas/operation_schema.yml')

    # Get the storage related parameters
    if args.storage:
        storage_config['storage'] = args.storage
    if storage_config['storage'] == 'local' and args.storage_root is None:
        raise Exception('"--storage-root" needs to be provided in arguments if storage is local')
    if args.storage_root:
        storage_config['storage_root'][storage_config['storage']] = args.storage_root
    if args.storage_loc:
        storage_config['storage_loc'] = args.storage_loc

    # Get the version related parameters
    available_versions = {x['version']: (x['tags'], x['tag_data_from']) for x
                          in version_config['available_versions_and_tags']}
    if args.version:
        if args.version in available_versions:
            version_config['current_version'] = args.version
        else:
            raise Exception('The specified version: %s for the datatool is not available' % args.version)

    # Get tag related parameters
    available_tags = available_versions[version_config['current_version']][0]
    required_tags = args.tags
    if required_tags:
        for t in required_tags:
            if t not in available_tags:
                raise Exception('Tag %s is not available for version %s' % (t, version_config['current_version']))
    else:
        required_tags = available_tags

    # Get operation related parameters
    if args.operation_mode:
        operation_config['operation_mode'] = args.operation_mode
    if args.validate_sample_paths:
        arg_map = {'y': True, 'n': False}
        operation_config['validate_sample_paths'] = arg_map[args.validate_sample_paths]

    # Create the output directory
    os.makedirs(args.output_dir, exist_ok=True)

    datatool_name = version_config['datatool_name']
    data_storage_resolver = SourceDataStorageResolver(storage_config['storage'],
                                                      storage_config['storage_loc'],
                                                      storage_config['storage_root'][storage_config['storage']],
                                                      datatool_name,
                                                      version_config['current_version'],
                                                      required_tags,
                                                      available_versions[version_config['current_version']][1],
                                                      args.output_dir,
                                                      available_versions[version_config['current_version']][0][0])

    # For each tag, process and create the datatool output
    for tag in required_tags:
        data_path = data_storage_resolver.resolve(tag)
        data_parser = SourceDataParser()
        datatool_patcher = DatatoolPatch()
        data_processor = DataProcessor(datatool_name, data_path, args.output_dir,
                                       version_config['current_version'], tag,
                                       source_data_parser=data_parser,
                                       datatool_patch_creator=datatool_patcher,
                                       operation_mode=operation_config['operation_mode'],
                                       validate_sample_paths=operation_config['validate_sample_paths'])
        data_processor.run()
        data_storage_resolver.cleanup(tag)

    print('Data tool processing completed')


if __name__ == '__main__':
    main()
