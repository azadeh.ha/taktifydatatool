import random
import argparse
import os
import inspect
import sys
import cv2

cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from common.plot_annotations.plot import plotUtils
from datatool_api.config.APIConfig import DTAPIConfig
from datatool_api.models.DTDataset import DTDataset
from datatool_api.models.ImageSample import ImageSample
from datatool_api.models.Categories import Categories
from common.overview_collage.generate_overview_collage import generate_overview_tiles
from custom_dataset_model import DTDatasetCustom
from custom_base_types import *

"""
Note: For the development purpose, developer can use the relative imports with ..common.plot, ..datatool_api.models 
      to enable auto-suggestions in IDE for the plotUtils, DTDataset and ImageSample APIs . If used, please remember to 
      remove the relative imports before committing this script as this script as standalone will not work due to 
      relative imports from inside the project. 
"""


class PlotAnnotations:

    @staticmethod
    def plot(input_dir: str, sample, output_dir: str):
        # TODO - Implement the plotting for the sample
        pass

    @staticmethod
    def run(input_dir, output_dir, sample_count, operation_mode):
        os.makedirs(output_dir, exist_ok=True)

        # Load dataset
        DTAPIConfig.disable_validation()
        dataset = DTDatasetCustom(name='dataset', operatingMode=operation_mode).load_from_json(os.path.join(
            input_dir, 'dataset.json'))

        # TODO - Randomly select "sample_count" samples from the dataset and send them for plotting

        print('Plotting for samples completed')


def main():
    parser = argparse.ArgumentParser(description='Plot the annotations on randomly drawn sample from datatool output')
    parser.add_argument('--input-dir', '-i', help='Input directory where datatool output is stored', required=True)
    parser.add_argument('--output-dir', '-o', help='Output directory', required=True)
    parser.add_argument('--operation-mode', '-om', choices=['memory', 'disk', 'ramdisk'], default='memory',
                        help='Operation mode for the datatool to load and process the datatool output')
    parser.add_argument('--sample-count', '-s', help='Number of samples', default=100, type=int)
    parser.add_argument('--random-seed', '-rs', type=int, help='Random seed value, provide for reproducibility')

    args = parser.parse_args()
    random.seed(args.random_seed)
    PlotAnnotations.run(args.input_dir, args.output_dir, args.sample_count, args.operation_mode)
    generate_overview_tiles(args.output_dir, args.random_seed, os.path.join(args.output_dir, 'collage'))


if __name__ == '__main__':
    main()
