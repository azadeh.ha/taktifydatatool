"""
This Module defines the custom data types for annotations available in the source dataset.
For more details pleaser refer to the [https://gitlab.com/bonseyes/artifacts/data_tools/apis/datatool-api]
"""

from __future__ import annotations
from typing import List, Dict
from datatool_api.data_validation.base_model import BaseModel
from datatool_api.data_validation.attribute import Attribute
from datatool_api.data_validation.containers.ValList import ValList
from datatool_api.models.BaseTypes import BoundingBox, Landmark2D, Segmentation, LandmarkLabel, SkeletonEdge
from datatool_api.models.Categories import Categories


class MRLDatasetPupil(BaseModel):
    """
    Represents metadata for the MRL dataset

    Attributes:
        subject_ID: str: Dataset subject_ID
        image_ID: str: image_ID for the dataset
        gender: int: Dataset gender
        glasses: int: glasses of creation
        eye_state: str: Date of publication
    """
    subject_ID: str = Attribute(type=str)
    image_ID: str = Attribute(type=str)
    gender: int = Attribute(type=int)
    glasses: int = Attribute(type=int)
    eye_state: int = Attribute(type=int)
    reflections: int = Attribute(type=int)
    lighting_condition: int = Attribute(type=int)
    sensor_ID: int = Attribute(type=int)
    image_name: str = Attribute(type=str)
    image_path: str = Attribute(type=str)

	
	
    def __init__(self, subject_ID: str = None, image_ID: str = None, gender: int = 0, glasses: int = 0,
                 eye_state: int = 0, reflections: int = 0 , lighting_condition: int = 0, sensor_ID: int = 0, image_name: str = None, image_path: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> MRLDatasetPupil:
        """
        Extract dataset info from MRLDatasetPupil object

        :param inputDict: Input dict containing the dataset info
        :return: MRLDatasetPupil
        """
        self.subject_ID = inputDict['subject_ID']
        self.image_ID = inputDict['image_ID']
        self.gender = inputDict['gender']
        self.glasses = inputDict['glasses']
        self.eye_state = inputDict['eye_state']
        self.reflections = inputDict['reflections']
        self.lighting_condition = inputDict['lighting_condition']
        self.sensor_ID = inputDict['sensor_ID']
        self.image_name = inputDict['image_name']
        self.image_path = inputDict['image_path']
        return self