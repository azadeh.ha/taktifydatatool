import argparse
from common.data_processing.datatool_patch_interface import DatatoolPatchInterface
from datatool_api.models.DTDataset import DTDataset
from custom_dataset_model import DTDatasetCustom
from typing import Union


class DatatoolPatch(DatatoolPatchInterface):

    """
    Implement the create_patched_dataset() method to provide a patch implementation. Add the code and
    remove "NotImplementedError" statement from the bottom if there is need to create a patched dataset
    """
    def create_patched_dataset(self, original_dataset: Union[DTDatasetCustom, DTDataset], operation_mode: str,
                               **kwargs) -> Union[DTDatasetCustom, DTDataset]:
        """
        Create and return a patched dataset model instance

        :param original_dataset: Original dataset model instance
        :param operation_mode: operation mode for the dataset to work with (memory, disk, ramdisk)
        :param kwargs: Any additional arguments needed by the patching code
        :return: Patched dataset model instance
        """
        raise NotImplementedError('No Patch Found: Not running any patch on top of datatool')


def main():
    parser = argparse.ArgumentParser(description='Datatool patch')
    parser.add_argument('--input-dir', '-i', required=True, help='Path to input directory containing dataset.json')
    parser.add_argument('--operation-mode', '-om', choices=['memory', 'disk', 'ramdisk'], default='memory',
                        help='Operation mode for the datatool API')
    parser.add_argument("--validate-sample-paths", "-vl", choices=['y', 'n'], default='y',
                        help="If sample paths must be validated while dumping the dataset json to disk")

    args = parser.parse_args()
    validate_sample_paths = True if args.validate_sample_paths == 'y' else False
    patch_generator = DatatoolPatch()
    patch_generator.run_patch(args.input_dir, args.operation_mode, validate_sample_paths, args.input_dir)


if __name__ == '__main__':
    main()
