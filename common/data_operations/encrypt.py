import argparse
import sys
import string

# AES 256 encryption/decryption using pycryptodome library
from base64 import b64encode, b64decode
import hashlib
import secrets
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# POSIX exit status codes https://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
from exitstatus import ExitStatus


def generate_private_key_salt(password):
    # generate a random salt
    salt = get_random_bytes(AES.block_size)

    # use the Scrypt KDF to get a private key from the password
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2 ** 14, r=8, p=1, dklen=32)

    return (private_key, salt)


def encrypt(plain_text, private_key, salt):
    # create cipher config
    cipher_config = AES.new(private_key, AES.MODE_GCM)

    # return a dictionary with the encrypted text
    cipher_text, tag = cipher_config.encrypt_and_digest(bytes(plain_text, 'utf-8'))
    return {
        'cipher_text': b64encode(cipher_text).decode('utf-8'),
        'salt': b64encode(salt).decode('utf-8'),
        'nonce': b64encode(cipher_config.nonce).decode('utf-8'),
        'tag': b64encode(tag).decode('utf-8')
    }


def decrypt(enc_dict, password):
    # decode the dictionary entries from base64
    salt = b64decode(enc_dict['salt'])
    cipher_text = b64decode(enc_dict['cipher_text'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])

    # generate the private key from the password and salt
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2 ** 14, r=8, p=1, dklen=32)

    # create the cipher config
    cipher = AES.new(private_key, AES.MODE_GCM, nonce=nonce)

    # decrypt the cipher text
    decrypted = cipher.decrypt_and_verify(cipher_text, tag)

    return decrypted


def parse_args():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser(description="Encrypt zip archive using AES-256 symetric cipher")
    args = parser.parse_args()

    return args


def run(args):
    """
    Run main program for encryption
    """

    # Generate private key and salt
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(20))
    (private_key, salt) = generate_private_key_salt(password)

    # Encrypt ZIP - TODO
    # zip_outpath = ...
    # encdict_outpath = ...
    # plain_text = load_zip()
    # enc_dict = encrypt(plain_text, private_key, salt):
    # save_encrypted_zip(enc_dict, zip_outpath)
    # save_encrypted_dict(enc_dict, encdict_outpath)

    return


def main():
    args = parse_args()

    try:
        run(args)
    except:
        sys.exit(ExitStatus.failure)

    sys.exit(ExitStatus.success)
