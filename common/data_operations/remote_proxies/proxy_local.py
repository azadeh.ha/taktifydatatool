import os
from .base_proxy import BaseProxy


class proxy_local(BaseProxy):
    def __init__(self, storage_root: str):
        self.storage_root = storage_root.rstrip('/')

    def upload_package(self, destination_dir_path: str, source_file_path: str) -> bool:
        return True

    def download_package(self, destination_dir_path: str, source_object_path: str) -> str:
        object_path = [self.storage_root]
        object_path.extend(source_object_path.strip('/').split('/'))
        while len(object_path) > 0:
            path = ('/'.join(object_path))
            if os.path.exists(path):
                return path
            object_path.pop()
        return ''

    def check_if_exists(self, object_path: str) -> bool:
        object_path = object_path.strip('/')
        object_dir = os.path.dirname(object_path)
        if (os.path.exists(os.path.join(self.storage_root, object_path)) or
                os.path.exists(os.path.join(self.storage_root, object_dir))):
            return True
        else:
            return False
