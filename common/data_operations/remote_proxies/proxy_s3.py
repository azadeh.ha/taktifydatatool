from .base_proxy import BaseProxy
import boto3
from botocore.exceptions import ClientError
import tqdm
import os


def hook(t):
    def inner(bytes_amount):
        t.update(bytes_amount)
    return inner


class proxy_s3(BaseProxy):
    def __init__(self, root_bucket: str):
        self.config = {'AWSBucket': root_bucket,
                       'AWSAccessKeyId': os.getenv('AWS_ACCESS_KEY'),
                       'AWSSecretKey': os.getenv('AWS_SECRET_KEY')
                       }
        # Fetch the credentials from environment variables
        if (self.config['AWSAccessKeyId'] is None or self.config['AWSAccessKeyId'] == '' or
                self.config['AWSSecretKey'] is None or self.config['AWSSecretKey'] == ''):
            raise Exception('Environment variables "AWS_ACCESS_KEY" and "AWS_SECRET_KEY" must be set')

        self.s3Client = boto3.client('s3', aws_access_key_id=self.config["AWSAccessKeyId"],
                                     aws_secret_access_key=self.config["AWSSecretKey"],
                                     use_ssl=True)

    def upload_package(self, destination_dir_path: str, source_file_path: str) -> bool:
        try:
            print('Uploading the object')
            statinfo = os.stat(source_file_path)
            pbar = tqdm.tqdm(unit="B", total=int(statinfo.st_size), unit_scale=True, unit_divisor=1024)
            object_name = os.path.join(destination_dir_path, os.path.basename(source_file_path))
            self.s3Client.upload_file(source_file_path, self.config['AWSBucket'], object_name, Callback=pbar.update)
            print('Upload Completed!')
            return True
        except ClientError as e:
            return False

    def download_package(self, destination_dir_path: str, source_object_path: str) -> str:
        try:
            print('Downloading the object')
            os.makedirs(destination_dir_path, exist_ok=True)
            response = self.s3Client.head_object(Bucket=self.config['AWSBucket'], Key=source_object_path)
            pbar = tqdm.tqdm(unit="B", total=int(response['ContentLength']), unit_scale=True, unit_divisor=1024)
            self.s3Client.download_file(self.config['AWSBucket'], source_object_path,
                                        os.path.join(destination_dir_path, os.path.basename(source_object_path)),
                                        Callback=pbar.update)
            print('Download Completed!')
            return os.path.join(destination_dir_path, os.path.basename(source_object_path))
        except ClientError as e:
            return ''

    def check_if_exists(self, object_path: str) -> bool:
        """
        Check if an object exists in s3 bucket under root bucket

        :param object_path: Object path relative to root bucket
        :return: bool
        """
        result = self.s3Client.list_objects(Bucket=self.config['AWSBucket'], Prefix=object_path)
        if 'Contents' in result:
            for obj in result['Contents']:
                if obj['Key'] == object_path:
                    return True
        return False
