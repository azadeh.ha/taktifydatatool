from typing import Union, ByteString
import os
import shutil


class DataParsingUtils:

    # Output directory where to store the parsed data
    output_dir = ''

    @staticmethod
    def save_sample(sample_name: str, sample_ext: str, samples_dir: str, sample: Union[str, ByteString]) -> str:
        """
        Save a sample to disk and return the saved path

        :param sample_name: Name of the sample
        :param sample_ext: Extension for the sample
        :param samples_dir: Root sample dir
        :param sample: Sample (either bytes or path to an extracted file)
        :return: str
        """
        os.makedirs(os.path.join(DataParsingUtils.output_dir, samples_dir, os.path.dirname(sample_name)), exist_ok=True)
        if type(sample) == bytes:
            with open(os.path.join(DataParsingUtils.output_dir, samples_dir, sample_name + sample_ext), 'wb') as w:
                w.write(sample)
        elif type(sample) == str:
            shutil.copy2(sample, os.path.join(DataParsingUtils.output_dir, samples_dir, os.path.dirname(sample_name)))
        return os.path.join(samples_dir, sample_name + sample_ext)
