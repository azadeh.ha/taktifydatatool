from typing import Generator, Tuple
from tqdm import tqdm
from zipfile import ZipFile
from datetime import datetime
import shutil
import os


class SourceDataReader:
    def __init__(self, dataset_path):
        self.dataset_path = dataset_path

        start_time = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        self._operating_dir = '{}_{}'.format('operation_dir', start_time)
        os.makedirs(self._operating_dir, exist_ok=True)

        self._current_path = None
        self._current_ext = None
        self._zip_reader = None
        self._zip_file = None
        self._in_zip = None

    def __del__(self):
        if os.path.exists(self._operating_dir):
            shutil.rmtree(self._operating_dir)

    def _traverse_dir(self, root_path):
        for root, dirs, dir_files in tqdm(os.walk(root_path)):
            cur_dir = root.replace(root_path, '').lstrip('/')
            for file in tqdm(dir_files):
                name, ext = os.path.splitext(file)
                if ext == '.zip':
                    zip_samples = self._traverse_zip(os.path.join(root, file), os.path.join(cur_dir, name))
                    for path, ext in zip_samples:
                        yield path, ext
                else:
                    self._in_zip = False
                    self._current_path, self._current_ext = os.path.join(cur_dir, name), ext
                    yield self._current_path, self._current_ext

    def _traverse_zip(self, zip_path, root_dir=''):
        self._in_zip = True
        zip_object = ZipFile(zip_path)
        for file in tqdm(zip_object.namelist()):
            name, ext = os.path.splitext(file)
            if ext == '.zip':
                zip_object.extract(file, path=self._operating_dir)
                zip_samples = self._traverse_zip(os.path.join(self._operating_dir, file),
                                                 os.path.join(root_dir, name))
                for path, ext in zip_samples:
                    yield path, ext
                os.remove(os.path.join(self._operating_dir, file))
            else:
                self._zip_reader, self._zip_file = zip_object, file
                self._current_path, self._current_ext = os.path.join(root_dir, name), ext
                yield self._current_path, self._current_ext

        zip_object.close()
        self._zip_reader = None

    def files(self) -> Generator[Tuple[str, str], None, None]:
        """
        Get a generator over all files present in the input dataset

        :return: Generator[(file_path: str, file_extension: str), None, None]
        """
        if os.path.isdir(self.dataset_path):
            return self._traverse_dir(self.dataset_path)
        else:
            return self._traverse_zip(self.dataset_path, '')

    def read_file(self, path: str, ext: str) -> bytes:
        """
        Read a file whose path and extension is returned by the files() method

        :param path: Path of the file
        :param ext: Extension of the file
        :return: ByteString containing file content
        """
        if self._current_path == path and self._current_ext == ext:
            if self._in_zip is True:
                if self._zip_reader is not None:
                    return self._zip_reader.read(self._zip_file)
                else:
                    raise Exception('Zip reader has already been closed, '
                                    'please read the file before the generator exhausted')
            else:
                with open(os.path.join(self.dataset_path, path + ext), 'rb') as r:
                    return r.read()
        else:
            raise Exception("A file can be read only once after its name and extension has been returned by files(), "
                            "When next file is returned by files(), previous files are not available for reading")

    def extract_file(self, path: str, ext: str) -> str:
        """
        Extract a file and return the path to the extracted file (useful for big files which can not be read)

        :param path: Path of the file
        :param ext: Extension of the file
        :return: string containing the extracted file path
        """
        if self._current_path == path and self._current_ext == ext:
            if self._in_zip is True:
                if self._zip_reader is not None:
                    self._zip_reader.extract(self._zip_file, path=self._operating_dir)
                    return os.path.join(self._operating_dir, self._zip_file)
                else:
                    raise Exception('Zip reader has already been closed, '
                                    'please extract the file before the generator exhausted')
            else:
                return os.path.join(self.dataset_path, path + ext)
        else:
            raise Exception("A file can be read only once after its name and extension has been returned by files(), "
                            "When next file is returned by files(), previous files are not available for reading")
