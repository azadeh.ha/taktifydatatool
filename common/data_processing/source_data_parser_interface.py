from abc import ABC, abstractmethod
from .source_data_reader import SourceDataReader


class SourceDataParserInterface(ABC):
    @abstractmethod
    def parse_file(self, reader: SourceDataReader, file_name: str, file_ext: str, dataset,
                   datatool_version: str, datatool_tag: str):
        pass

    @abstractmethod
    def extract(self, dataset_path: str, dataset, datatool_version: str, datatool_tag: str, output_dir: str):
        pass

    @abstractmethod
    def post_process(self, dataset, datatool_version: str, datatool_tag: str):
        pass
