# Create an overview collage from randomly drawn images from an input directory
This utility lets the user draw images at random from an input directory containing the images and generate an overview collage based on them.  


## Installation: 

### Prerequisites
Ensure the system has the following dependencies: 
* Python 3.9 or higher

## Usage 

#### Virtual Environment (Optional)
It's strongly advised that you create [Python virtual environment](https://docs.python.org/3/library/venv.html) and install dependencies from [requirements.txt](requirements.txt) 
within the environment. This prevents risk of breaking existing system-wide packages. 

To create a virtual environment using Python's builtin `venv` module and activate:
```
python3 -m venv </path/to/new/virtual/environment>
source </path/to/new/virtual/environment>/bin/activate
```

#### Python Packages (Required)
To install the dependencies that are described in [requirements.txt](requirements.txt) inside the virtual environment:
```
pip3 install -r requirements.txt 
```

#### Generate Overview Collage
Execute the `generate_overview_collage.py` script to plot the annotations on randomly drawn samples from datatool output
```
python3 generate_overview_collage.py \
    --image-dir <IMAGE_DIRECTORY> \
    --output-dir <OUTPUT_DIRECTORY> \
    --random-seed <RANDOM-SEED>
```
Required arguments:
```
    --image-dir, -i        Directory containing the images
    --output-dir, -o       Output directory where to save the generated overview tile
```

Optional arguments:
```
    --random-seed, -rs    Random seed value, provide it for reproducibility
```
