import argparse
import os
import cv2
import random
from imutils import build_montages


COLS = 4
ROWS = 3
FIXED_TILE_WIDTH = 200
FIXED_TILE_HEIGHT = 150


def select_images(input_dir):
    samples = os.listdir(input_dir)
    selected = []
    num = COLS * ROWS
    idx = random.sample(range(len(samples)), num)
    for i in idx:
        selected.append(samples[i])
    return selected


def create_overview_tile(images, image_dir, output_dir):
    read_images = []

    for img_rel_path in images:
        img_path = os.path.join(image_dir, img_rel_path)
        img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        read_images.append(img)

    im_shape = (FIXED_TILE_WIDTH, FIXED_TILE_HEIGHT)
    montage_shape = (COLS, ROWS)
    montages = build_montages(read_images, im_shape, montage_shape)

    output_path = os.path.join(output_dir, 'overview_collage.png')
    cv2.imwrite(output_path, montages[0])


def generate_overview_tiles(image_dir, seed, output_dir):
    random.seed(seed)
    selected_images = select_images(image_dir)
    os.makedirs(output_dir, exist_ok=True)
    create_overview_tile(selected_images, image_dir, output_dir)
    print(f'Tile saved in {output_dir}')


def main():
    parser = argparse.ArgumentParser(description='Generate overview tiles')
    parser.add_argument('--image-dir', '-i', required=True, help='Path to directory with images')
    parser.add_argument('--random-seed', '-rs', type=int, help='random seed, provide for reproducibility')
    parser.add_argument('--output-dir', '-o', required=True, help="Output directory to store resulting image")

    args = parser.parse_args()
    generate_overview_tiles(args.image_dir, args.random_seed, args.output_dir)


if __name__ == '__main__':
    main()
