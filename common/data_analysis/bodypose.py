import numpy as np
import pandas as pd

from typing import Union, Callable


def _compute_full_sequence_joint_deviation(df_kp: pd.DataFrame) -> pd.DataFrame:
	"""
	Compute the joint deviation for the full frame sequence.
	
	Parameters
	----------
	df_kp : pandas DataFrame
		DataFrame with columns "subject", "frame", "idx", "x", "y", "z".
	
	Returns
	------
	df_kp_dist : pandas DataFrame
		DataFrame with columns "subject", "frame", "idx", "dist". Column "dist" contains the deviation of the joints for
		the pose of each subject at each frame.
	"""
	
	coord_list = ["x","y","z"]
	avg_coord_dict = {coord: coord + "_" for coord in coord_list}
	diff_coord_dict = {coord: coord + "_diff" for coord in coord_list}
	
	df_kp_seq_avg = df_kp.groupby(["subject","idx"])[coord_list].mean()
	df_kp_seq_avg = df_kp_seq_avg.rename(avg_coord_dict, axis=1)
	
	df_kp_dist = df_kp.set_index(df_kp_seq_avg.index.names)
	df_kp_dist = df_kp_dist.merge(df_kp_seq_avg, left_index=True, right_index=True)
	for coord in coord_list:
		df_kp_dist[diff_coord_dict[coord]] = df_kp_dist[coord] - df_kp_dist[avg_coord_dict[coord]]
	df_kp_dist["dist"] = np.linalg.norm(df_kp_dist[diff_coord_dict.values()], axis=1)
	
	df_kp_dist = df_kp_dist.reset_index()[["subject","frame","idx","dist"]]
	return df_kp_dist


def compute_full_sequence_avg_joint_deviation(df_kp: pd.DataFrame) -> pd.DataFrame:
	"""
	Compute the average joint deviation for the full frame sequence.
	
	Parameters
	----------
	df_kp : pandas DataFrame
		DataFrame with columns "subject", "frame", "idx", "x", "y", "z".
	
	Returns
	------
	df_kp_dist : pandas DataFrame
		DataFrame with columns "subject", "frame", "idx", "dist". Column "dist" contains the average deviation of the
		joints for the pose of each subject at each frame.
	"""
	
	df_kp_dist = _compute_full_sequence_joint_deviation(df_kp)
	df_kp_dist = df_kp_dist.groupby(["subject","frame","idx"])["dist"].mean().reset_index()
	return df_kp_dist


def compute_full_sequence_avg_pose_deviation(df_kp: pd.DataFrame) -> pd.DataFrame:
	"""
	Compute the average pose deviation for the full frame sequence.
	
	Parameters
	----------
	df_kp : pandas DataFrame
		DataFrame with columns "subject", "frame", "idx", "x", "y", "z".
	
	Returns
	------
	df_kp_dist : pandas DataFrame
		DataFrame with columns "subject", "frame", "dist". Column "dist" contains the average deviation of the pose of
		each subject at each frame.
	"""
	
	df_kp_dist = _compute_full_sequence_joint_deviation(df_kp)
	df_kp_dist = df_kp_dist.groupby(["subject","frame"])["dist"].mean().reset_index()
	return df_kp_dist


def _get_upper_outliers(df: pd.DataFrame, col: str, iqr_factor: float = 1.5) -> pd.DataFrame:
	Q1 = df[col].quantile(0.25)
	Q3 = df[col].quantile(0.75)
	IQR = Q3 - Q1
	return df[df[col] > (Q3 + iqr_factor * IQR)]


def _get_incorrect(data_dict: dict[str, list],
                   threshold: Union[str, float],
				   deviation_function: Callable[[pd.DataFrame], pd.DataFrame],
				   output_dict_format: str) -> dict[str, dict]:
				   
	if not isinstance(data_dict, dict):
		raise ValueError("Argument 'data_dict' must be a dictionary.")
	
	if not set(("subject", "frame", "idx", "x", "y", "z")).issubset(data_dict.keys()):
		raise ValueError("Argument 'data_dict' must contain all following keys: 'subject', 'frame', 'idx', 'x', 'y', 'z'.")
	
	if threshold != "outliers" and not isinstance(threshold, float):
		raise Exception("Argument 'threshold' must either be the string 'outliers' or a float.")
	
	df_kp = pd.DataFrame(data_dict)
	df_kp = df_kp.dropna()
	df_kp_dist = deviation_function(df_kp)
	
	if threshold == "outliers":
		df_kp_outliers = df_kp_dist.groupby(["subject"]) \
		                           .apply(lambda df_subject: _get_upper_outliers(df_subject, "dist")) \
		                           .reset_index(drop=True)
	else:
		df_kp_outliers = df_kp_dist[df_kp_dist["dist"] > threshold]
	
	return df_kp_outliers.to_dict(orient=output_dict_format)


def get_incorrect_joints(data_dict: dict[str, list],
                         threshold: Union[str, float] = "outliers",
						 output_dict_format: str = "dict") -> dict[str, dict]:
	"""
	Detect joints that are significantly distant from their average for the full sequence of each subject.
	
	Parameters
	----------
	data_dict : dict
		Dictionary containing the columns for "subject", "frame", "idx", "x", "y", "z". Data points with null values
		will be ignored.
	threshold : str | float
		If "outliers", use the common definition of outliers datapoint for detecting incorrect joints (further than 1.5
		times the inter-quartile range from the average).
		If float, use the value directly as a threshold on the distance from the average.
		(default: "threshold")
	output_dict_format : str
		Parameter to be passed to 'pandas.DataFrame.to_dict' as 'orient' parameter.
	
	Returns
	-------
	outliers_dict : dict
		Column-wise or record-wise dictionary with columns "subject", "frame", "idx", "dist" for the detected incorrect
		poses, depending on the parameter 'output_dict_format'.
	"""
	
	return _get_incorrect(data_dict, threshold, compute_full_sequence_avg_joint_deviation, output_dict_format)


def get_incorrect_poses(data_dict: dict[str, list],
                        threshold: Union[str, float] = "outliers",
						output_dict_format: str = "dict") -> dict[str, dict]:
	"""
	Detect poses that are significantly distant from the average pose for the full sequence of each subject.
	
	Parameters
	----------
	data_dict : dict
		Dictionary containing the columns for "subject", "frame", "idx", "x", "y", "z". Data points with null values
		will be ignored.
	threshold : str | float
		If "outliers", use the common definition of outliers datapoint for detecting incorrect poses (further than 1.5
		times the inter-quartile range from the average).
		If float, use the value directly as a threshold on the distance from the average.
		(default: "threshold")
	output_dict_format : str
		Parameter to be passed to 'pandas.DataFrame.to_dict' as 'orient' parameter.
	
	Returns
	-------
	outliers_dict : dict
		Column-wise or record-wise dictionary with columns "subject", "frame", "dist" for the detected incorrect joints,
		depending on the parameter 'output_dict_format'.
	"""
	
	return _get_incorrect(data_dict, threshold, compute_full_sequence_avg_pose_deviation, output_dict_format)
