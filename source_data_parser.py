import os
import json
from common.data_processing.source_data_reader import SourceDataReader
from common.data_processing.source_data_parser_interface import SourceDataParserInterface
from common.data_processing.data_parsing_utils import DataParsingUtils
from datatool_api.models.BaseTypes import *
from datatool_api.models.Subject import Subject
from datatool_api.models.Object import Object
from datatool_api.models.ImageSample import ImageSample
from datatool_api.models.VideoInterval import VideoInterval
from datatool_api.models.VideoSample import VideoSample
from datatool_api.models.AudioSample import AudioSample
from datatool_api.models.Categories import Categories
from datatool_api.models.GlobalAnnotations import GlobalAnnotations
from datatool_api.models.DTDataset import DTDataset
import shutil
from tqdm import tqdm
from custom_base_types import *
from custom_dataset_model import DTDatasetCustom
import codecs


class SourceDataParser(SourceDataParserInterface):
    def __init__(self):
        self.samples_dir = 'sample_files'

        # Add any needed local variables here

    """
    Implement parse_file() method if you are working with NVISO provided data model and base types.
    Uncomment and finish the example code and remove "NotImplementedError" statement from the end in order to provide 
    the implementation.
    """
    def parse_file(self, reader: SourceDataReader, file_name: str, file_ext: str, dataset: DTDataset,
                   datatool_version: str, datatool_tag: str):
        """
        Parse the source dataset file by file, where files are received in this method in a random order.
        The goal of this methods is to read the file if needed using "reader" and extract the annotations and fill the
        provided "dataset" instance with the annotations. If it's not possible to create the annotations in this method,
        store the extracted annotations in some auxiliary storage and use the "post_process" method to finally add
        annotations to the dataset.

        :param reader: The reader instance which will return the file contents if the file needs to be read
        :param file_name: Name of the file
        :param file_ext: Extension of the file
        :param dataset: NVISO dataset model instance
        :param datatool_version: datatool version which is under processing
        :param datatool_tag: datatool tag which is under processing
        :return: None
        """
        
            # file_name.replace('.txt', '.json')


       
        raise NotImplementedError

    """
    Implement post_process() method if you are working with NVISO provided data model and annotations could not be 
    created in parse_file method(). 
    Use this method to consolidate the extracted annotations from auxiliary storage and add them to NVISO dataset model 
    instance "dataset" provided in the method arguments.
    """
    def post_process(self, dataset: DTDataset, datatool_version: str, datatool_tag: str):
        """
        Any post processing that needs to happen after parse_file() has finished parsing through all source dataset
        files

        :param dataset: NVISO dataset model instance
        :param datatool_version: datatool version which is under processing
        :param datatool_tag: datatool tag which is under processing
        :return: None
        """
        # ADD any post-processing code here which you need to run after going through all the files
        # Make cases for different tags if required like "parse_file()"
        pass

    """
    Implement extract() method if you are working with custom data model and base models.
    Uncomment and finish the example code and remove "NotImplementedError" statement from the end in order to provide 
    the implementation
    """
    def extract(self, dataset_path: str, dataset: DTDatasetCustom, datatool_version: str, datatool_tag: str,
                output_dir: str):
        """
        Extract the annotations and add them to the custom data model instance

        :param dataset_path: path to the source dataset
        :param dataset: custom data model instance
        :param datatool_version: datatool version under processing
        :param datatool_tag: datatool tag under processing
        :param output_dir: Output directory
        :return: None
        """
        filestate = 0
        os.makedirs(os.path.join(output_dir, self.samples_dir), exist_ok=True)
        if datatool_version in ['original_update00']:
            for root, dirs, files in os.walk(dataset_path):
                for file in files:
                    name, ext = os.path.splitext(file)
                    if datatool_tag == 'train':
                      if ext in ['.png'] and 'mrlEyes_2018_01' in root:
                            shutil.copy(os.path.join(root, file), os.path.join(output_dir, self.samples_dir))
                #print("root"+root)
                if (root.find("mrlEyes_2018_01") != -1 and filestate == 0):
                    #print("folder")
                    mrlEyes = list(filter(lambda item: len(item) == 5, os.listdir(root)))
                    rawData = []
                    for folder in mrlEyes:
                        rawData.extend(list(map(lambda row: folder + '/' + row, os.listdir(root + '/' + folder))))
                    with open(os.path.join(root, os.pardir)+'/all_images_db.txt', 'w') as outFile:
                        for row in rawData:
                          outFile.write(row)
                          if row != rawData[-1]:
                            outFile.write('\n')
                        filestate = 1
                    self.parse_txt(os.path.join(root, os.pardir) +'/all_images_db.txt', dataset)


    @staticmethod
    def parse_txt(txt_path, dataset):
        """
        Parse and input MRL json to extract and add the annotations to custom dataset model object

        :param json_path: Path to the json file
        :param dataset: Custom dataset model instance
        :return: None
        """
        with open(txt_path, "r")as r:
            with tqdm(desc="MRL Dataset Extraction") as progress_bar:
                progress_bar.set_postfix({"Step": 'Load source dataset'})
                inputData = r.readlines()
                source_data = list(map(lambda item: {"subject_ID": item.split("/")[1].split("_")[0],
                                                          "image_ID": item.split("/")[1].split("_")[1],
                                                         "gender": int(item.split("/")[1].split("_")[2]),
                                                         "glasses": int(item.split("/")[1].split("_")[3]),
                                                         "eye_state": int(item.split("/")[1].split("_")[4]),
                                                         "reflections": int(item.split('/')[1].split("_")[5]),
                                                         "lighting_condition": int(item.split("/")[1].split("_")[6]),
                                                         "sensor_ID": int(item.split("/")[1].split("_")[7][0: 2]),
                                                         "image_name": item.split("/")[1][: item.split("/")[1].index(".png") + 4],
                                                         "image_path": item[: item.index(".png") + 4]}, inputData))
                progress_bar.update()
                progress_bar.set_postfix({"Step": 'Extracting info'})
                for i in tqdm(source_data):
                    print(i)
                    dataset_obj = MRLDatasetPupil().extract(i)
                    dataset.info.add(dataset_obj.image_ID, dataset_obj)

                progress_bar.update()

